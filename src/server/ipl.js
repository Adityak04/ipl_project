
getmatchesPerYear = (matches)=> {
    return matches.reduce((matchesPerYear, el)=> {
        matchesPerYear[el.season] ? matchesPerYear[el.season] ++ : matchesPerYear[el.season] = 1;
        return matchesPerYear;
    }, {});
    }

getwinnerPerYear = (matches) =>{
return matches.reduce((winnerPerYear,matches)=> {
    if (winnerPerYear[matches.season]) {
        if (winnerPerYear[matches.season][matches.winner]) {
            winnerPerYear[matches.season][matches.winner] ++;
        }
        else {
            winnerPerYear[matches.season][matches.winner] = 1;
        }
    }else {
        winnerPerYear[matches.season] = {};
        winnerPerYear[matches.season][matches.winner] = 1;
    }
    return winnerPerYear;
},{});
}


extrarunPerTeam = (matches, deliveries) => { 
let id = (matches.filter(el =>
el.season == 2016).map((el) => el.id));
return deliveries.reduce((extrarun,deliveries) => {
    if(id.includes(deliveries.match_id)) {
            if (extrarun[deliveries.bowling_team]) {
                extrarun[deliveries.bowling_team] += parseInt(deliveries.extra_runs);
            }
            else {
                extrarun[deliveries.bowling_team] =  parseInt(deliveries.extra_runs);
            }
        }
        return extrarun;
    },{})
}


economicalBowler = (deliveries) => {
    let bowler = deliveries.reduce((bowler,deliveries)=>{
           if(deliveries.match_id <= 576 && deliveries.match_id >= 518)
           {
               if(bowler[deliveries.bowler])
               {
                   if(bowler[deliveries.bowler]['run'])
                   {
                    bowler[deliveries.bowler]['run'] += parseInt(deliveries.total_runs) - parseInt(deliveries.bye_runs) 
                 - parseInt(deliveries.legbye_runs); 
                   }
                   else{
                    bowler[deliveries.bowler]['run'] = parseInt(deliveries.total_runs) - parseInt(deliveries.bye_runs) 
                    - parseInt(deliveries.legbye_runs);                        
                   }
                   if (parseInt(deliveries.wide_runs) == 0 && parseInt(deliveries.noball_runs) == 0) {
                    bowler[deliveries.bowler]['overs'] = (bowler[deliveries.bowler]['overs'] || 0) + (1 / 6); 
                  }
               }
               else {
                bowler[deliveries.bowler] = {};
               } 
           }
           return bowler;
       },{});
       const bowlerEco = Object.entries(bowler).reduce((bowlerEco, bowler) => {
           if(bowler[1].overs >= 2) {
               bowlerEco[bowler[0]] = bowler[1]['run'] / bowler[1]['overs'];
           }
           return bowlerEco;
       }, {});
       return Object.entries(bowlerEco).sort((a,b)=>a[1]-b[1]).slice(0,10);
    }
